package ui;

import communication.WebSocketCommunicator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TerminalUI {
    private boolean isRunning = true;

    private final InputStreamReader inputStreamReader = new InputStreamReader(System.in);
    private final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    private WebSocketCommunicator socket;

    private String userName;

    public TerminalUI() {
        //socket = new WebSocketCommunicator(this);
    }

    /**
     * Initialize the chat application, we retrieve the username, and then we start to listen for incoming messages.
     */
    public void run() {
        try {
            retrieveUsername();

            socket.startListen();
            printMenu();

            while (isRunning) {
                selectOption();
            }
        } catch (IOException e) {
            error(e);
        } finally {
            close();
        }
    }

    private void retrieveUsername() throws IOException {
        System.out.println("Lets get started! Please insert your username...");
        userName = bufferedReader.readLine();
        clearConsole();
    }

    private void close() {
        try {
            socket.stopListen();
        } catch (Exception e) {
            error(e);
        }
        System.out.println("-- Have a nice day!");
        System.exit(0);
    }

    private void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private void printMenu() {
        System.out.println("--- Hello " + userName + " and welcome to the race-condition chat ---");
        System.out.println("It's not easy to get your message across in this application, but lets try");
        System.out.println("Type 'exit' to close the application (if you'r fast enough)");
        System.out.println("--- ");
    }

    private void selectOption() {
        String input = "";
        try {
            input = bufferedReader.readLine();

            switch (input) {
                case "1":
                    input = "omg vilken jobbig chat!";
                    break;
                case "w":
                    input = "jobbig!";
                    break;
                case "3":
                    input = "Det var fint väder idag!";
                    break;
                case "4":
                    input = "MAT NU!!";
                    break;
                case "5":
                    input = "Ska vi ta lunch snart?";
                    break;
                case "6":
                    input = "Klart vi skall det";
                    break;
                case "7":
                    input = "göm dig!";
                    break;
                case "8":
                    input = "ok det var bra det";
                    break;
                case "10":
                    input = "OJOJOJ";
                    break;
                case "o":
                    input =  "lets talk about that";
                    break;
                case "10":
                    input = "Vem e Margo b";
                    break;
                case "exit":
                    isRunning = false;
                    break;
            }

            if (isRunning) {
                socket.sendChat(userName, input);
            }
        } catch (Exception ex) {
            error(ex);
        }
    }


    public void showMessage(String message) {
        System.out.println(getTimestamp() + "- " + message);
    }

    public void error(Exception e) {
        System.out.println(e.getMessage());
    }

    private String getTimestamp() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

        return df.format(date);
    }

}
